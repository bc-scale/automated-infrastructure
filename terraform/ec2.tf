data "aws_ami" "main" {
  most_recent = true
  owners = [var.owner]

  filter {
    name   = "name"
    values = ["ubuntu/images/hvm-ssd/ubuntu-xenial-16.04-amd64-server-*"]
  }
}

data "template_file" "main" {
  template = "${file("${path.module}/install_nginx.tpl")}"
}

resource "aws_instance" "main" {
  ami           = data.aws_ami.main.id
  instance_type = "t2.micro"
  vpc_security_group_ids = [aws_security_group.main.id]
 
  associate_public_ip_address=true
  key_name = var.key_name
  user_data = data.template_file.main.rendered

  tags = {
    Name = "talk-gitlab-deploy"
  }
}

resource "aws_security_group" "main" {
  name          = "sg_automated-infraestructure"

  ingress {
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
}
